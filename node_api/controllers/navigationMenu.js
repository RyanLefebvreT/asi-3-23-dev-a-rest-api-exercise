import { createError } from "../error.js";
import NavigationMenu from "../models/NavigationMenu.js";

export const getNM = async (req, res, next) => {
  try {
    const nm = await NavigationMenu.find();
    res.status(200).json(nm);
  } catch (error) {
    next(error);
  }
};

export const getNMById = async (req, res, next) => {
  try {
    const nm = await NavigationMenu.findById(req.params.id);

    res.status(200).json(nm);
  } catch (error) {
    next(error);
  }
};

export const addNM = async (req, res, next) => {
  try {
    const nm = new Page({
      ...req.body,
    });
    await nm.save();
    res.status(200).send("NavigationMenu has been created!");
  } catch (error) {
    next(error);
  }
};

export const deleteNM = async (req, res, next) => {
  try {
    await NavigationMenu.findByIdAndDelete(req.params.id);
    res.status(200).json("NavigationMenu has been deleted.");
  } catch (error) {
    next(error);
  }
};

export const updateNM = async (req, res, next) => {
  try {
    const nm = await NavigationMenu.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      { new: true }
    );
    res.status(200).json(nm);
  } catch (error) {
    next(error);
  }
};
