import { createError } from "../error.js";
import Form from "../models/Form.js";

export const getForms = async (req, res, next) => {
  try {
    const form = await Form.find();
    res.status(200).json(form);
  } catch (error) {
    next(error);
  }
};

export const getFormById = async (req, res, next) => {
  try {
    const form = await Form.findById(req.params.id);

    res.status(200).json(form);
  } catch (error) {
    next(error);
  }
};

export const addForm = async (req, res, next) => {
  try {
    const newForm = new Form({
      ...req.body,
    });
    await newForm.save();
    res.status(200).send("Form has been created!");
  } catch (error) {
    next(error);
  }
};

export const deleteForm = async (req, res, next) => {
  try {
    await Form.findByIdAndDelete(req.params.id);
    res.status(200).json("Form has been deleted.");
  } catch (error) {
    next(error);
  }
};

export const updateForm = async (req, res, next) => {
  try {
    const updatedForm = await Form.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      { new: true }
    );
    res.status(200).json(updatedForm);
  } catch (error) {
    next(error);
  }
};
