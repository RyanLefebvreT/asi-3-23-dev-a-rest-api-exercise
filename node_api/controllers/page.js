import { createError } from "../error.js";
import Page from "../models/Page.js";

export const getPages = async (req, res, next) => {
  try {
    const page = await Page.find();
    res.status(200).json(page);
  } catch (error) {
    next(error);
  }
};

export const getPageById = async (req, res, next) => {
  try {
    const page = await Page.findById(req.params.id);

    res.status(200).json(page);
  } catch (error) {
    next(error);
  }
};

export const addPage = async (req, res, next) => {
  try {
    const newPage = new Page({
      ...req.body,
    });
    await newPage.save();
    res.status(200).send("Page has been created!");
  } catch (error) {
    next(error);
  }
};

export const deletePage = async (req, res, next) => {
  try {
    await Page.findByIdAndDelete(req.params.id);
    res.status(200).json("Page has been deleted.");
  } catch (error) {
    next(error);
  }
};

export const updatePage = async (req, res, next) => {
  try {
    const updatedPage = await Page.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      { new: true }
    );
    res.status(200).json(updatedPage);
  } catch (error) {
    next(error);
  }
};
