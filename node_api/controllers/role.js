import { createError } from "../error.js";
import Role from "../models/Role.js";

export const getRoles = async (req, res, next) => {
  try {
    const role = await Role.find();
    res.status(200).json(role);
  } catch (error) {
    next(error);
  }
};

export const getRoleById = async (req, res, next) => {
  try {
    const role = await Role.findById(req.params.id);

    res.status(200).json(role);
  } catch (error) {
    next(error);
  }
};

export const addRole = async (req, res, next) => {
  try {
    const newRole = new Role({
      name: req.body.name,
      permissions: req.body.permissions,
    });
    await newRole.save();
    res.status(200).send("Role has been created!");
  } catch (error) {
    next(error);
  }
};

export const deleteRole = async (req, res, next) => {
  try {
    await Role.findByIdAndDelete(req.params.id);
    res.status(200).json("Role has been deleted.");
  } catch (error) {
    next(error);
  }
};

export const updateRole = async (req, res, next) => {
  try {
    const updatedRole = await Role.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      { new: true }
    );
    res.status(200).json(updatedRole);
  } catch (error) {
    next(error);
  }
};
