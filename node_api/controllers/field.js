import { createError } from "../error.js";
import Field from "../models/Field.js";

export const getFields = async (req, res, next) => {
  try {
    const field = await Field.find();
    res.status(200).json(field);
  } catch (error) {
    next(error);
  }
};

export const getFieldById = async (req, res, next) => {
  try {
    const field = await Field.findById(req.params.id);

    res.status(200).json(field);
  } catch (error) {
    next(error);
  }
};

export const addField = async (req, res, next) => {
  try {
    const newField = new Field({
      ...req.body,
    });
    await newField.save();
    res.status(200).send("Field has been created!");
  } catch (error) {
    next(error);
  }
};

export const deleteField = async (req, res, next) => {
  try {
    await Field.findByIdAndDelete(req.params.id);
    res.status(200).json("Field has been deleted.");
  } catch (error) {
    next(error);
  }
};

export const updateField = async (req, res, next) => {
  try {
    const updatedField = await Field.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      { new: true }
    );
    res.status(200).json(updatedField);
  } catch (error) {
    next(error);
  }
};
