import express from 'express';
import {
  addNM,
  deleteNM,
  getNM,
  getNMById,
  updateNM,
} from '../controllers/navigationMenu.js';

const router = express.Router();

router.get('/', getNM);
router.get('/:id', getNMById);
router.post('/', addNM);
router.delete('/delete/:id', deleteNM);
router.put('/:id', updateNM);

export default router;
