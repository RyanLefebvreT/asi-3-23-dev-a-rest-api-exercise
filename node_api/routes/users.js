import express from 'express';
import {
  deleteUser,
  getUserById,
  getUsers,
  updateUser,
} from '../controllers/user.js';

const router = express.Router();

router.get('/', getUsers);
router.get('/:id', getUserById);
router.delete('/delete/:id', deleteUser);
router.put('/:id', updateUser);

export default router;
