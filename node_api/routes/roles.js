import express from 'express';
import {
  addRole,
  deleteRole,
  getRoleById,
  getRoles,
  updateRole,
} from '../controllers/role.js';

const router = express.Router();

router.get('/', getRoles);
router.get('/:id', getRoleById);
router.post('/', addRole);
router.delete('/delete/:id', deleteRole);
router.put('/:id', updateRole);

export default router;
