import express from "express";
import {
  addField,
  deleteField,
  getFieldById,
  getFields,
  updateField,
} from "../controllers/field.js";

const router = express.Router();

router.get("/", getFields);
router.get("/:id", getFieldById);
router.post("/", addField);
router.delete("/delete/:id", deleteField);
router.put("/:id", updateField);

export default router;
