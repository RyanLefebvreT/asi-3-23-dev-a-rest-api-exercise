import express from "express";
import {
  addForm,
  deleteForm,
  getFormById,
  getForms,
  updateForm,
} from "../controllers/form.js";

const router = express.Router();

router.get("/", getForms);
router.get("/:id", getFormByIdById);
router.post("/", addForm);
router.delete("/delete/:id", deleteForm);
router.put("/:id", updateForm);

export default router;
