import express from 'express';
import {
  addPage,
  deletePage,
  getPageById,
  getPages,
  updatePage,
} from '../controllers/page.js';

const router = express.Router();

router.get('/', getPages);
router.get('/:id', getPageById);
router.post('/', addPage);
router.delete('/delete/:id', deletePage);
router.put('/:id', updatePage);

export default router;
