import mongoose from "mongoose";

const FieldSchema = new mongoose.Schema({
  type: {
    required: true,
    type: String,
  },
  options: {
    type: String,
  },
  label: {
    required: true,
    type: String,
  },
  defaultValue: {
    type: String,
    required: true,
    default: "",
  },
});

export default mongoose.model("Field", FieldSchema);
