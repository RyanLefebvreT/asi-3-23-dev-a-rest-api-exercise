import mongoose from "mongoose";

const { Schema } = mongoose;

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  roles: {
    type: Schema.Types.ObjectId,
    ref: "Role",
    required: true,
    default: "64046f02416f210746331eaf",
  },
  lastname: {
    type: String,
    required: true,
  },
  firstname: {
    type: String,
    required: true,
  },
});

export default mongoose.model("User", UserSchema);
