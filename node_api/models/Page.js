import mongoose from "mongoose";

const { Schema } = mongoose;

const PageSchema = new mongoose.Schema(
  {
    title: {
      required: true,
      type: String,
    },
    content: {
      required: true,
      type: String,
    },
    urlSluge: {
      required: true,
      unique: true,
      type: String,
    },
    creator: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    usersModif: {
      user: [
        {
          type: Schema.Types.ObjectId,
          ref: "User",
        },
      ],
    },
    status: {
      required: true,
      type: String,
    },
  },
  { timestamps: true }
);

export default mongoose.model("Page", PageSchema);
