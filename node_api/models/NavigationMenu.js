import mongoose from "mongoose";

const NavigationMenuSchema = new mongoose.Schema({
  name: {
    required: true,
    type: String,
  },
});

export default mongoose.model("NavigationMenu", NavigationMenuSchema);
