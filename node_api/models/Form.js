import mongoose from "mongoose";

const FormSchema = new mongoose.Schema({
  name: {
    required: true,
    type: String,
  },
});

export default mongoose.model("Form", FormSchema);
