import mongoose from "mongoose";

const RoleSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
  permissions: {
    type: Array,
    required: true,
    default: ["read"],
  },
});

export default mongoose.model("Role", RoleSchema);
